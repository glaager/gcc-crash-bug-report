Use 
```bash
cmake -B cmake-build-debug -DCMAKE_BUILD_TYPE=Debug 
cmake --build cmake-build-debug --target problem-2-9
```
to compile and
```
./cmake-build-debug/problem-2-9
```
to run the project. 

When I copy paste `blockdecomp.hpp` into CodeExpert (Project Name "Problem 2-9: Partitioned Matrix - Student attempt")
I get this error from g++

```
Compiling ...
g++: internal compiler error: File size limit exceeded signal terminated program cc1plus
Please submit a full bug report,
with preprocessed source if appropriate.
See <http://bugzilla.redhat.com/bugzilla> for instructions.
Compilation failed
```

If I compile localy on my machine with the commands provided and 
g++ (GCC) 14.2.1 20240910 it compiles just fine (after a while).

Further information about my machine: 
```
Kernel: 6.6.52-1-lts arch: x86_64 bits: 64 compiler: gcc
Distro: Garuda (2.6.26-1) base: Arch Linux
```
