# 2-9.a
$$
\begin{pmatrix}
R   &  v \\
u^T & 0 \\
\end{pmatrix}
$$

$$
\begin{pmatrix}
R   &  v \\
0^T & 0 - u^T R^{-1} v \\
\end{pmatrix}
$$

$\implies A$ is regular $\iff$ $u^T R^{-1} v \ne 0$ 

# 2-9.b
$$
\begin{pmatrix}
R   &  v & b \\
0^T & 0 - u^T R^{-1} v & \beta - u^T R^{-1} b \\
\end{pmatrix}
$$

$\zeta = \frac{u^T R^{-1} b - \beta}{u^T R^{-1} v}$
$z = R^{-1}(b - \zeta v)$

