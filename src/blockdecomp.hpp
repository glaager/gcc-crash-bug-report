#ifndef BLOCKDECOMP_HPP
#define BLOCKDECOMP_HPP

#include <Eigen/Dense>
#include <cassert>
#include <cctype>
#include <iostream>

namespace ei = Eigen;

/**
 * @brief Use efficient implementation A*x = bb
 *
 * @param R MatrixXd is nxn and upper triangular
 * @param v VectorXd is nx1
 * @param u VectorXd is nx1
 * @param bb vector is (n+1)x1 and is stacked (b, \beta)^T =: b
 * @param x solution A*bb = x
 */
/* SAM_LISTING_BEGIN_1 */
void solvelse(const Eigen::MatrixXd& R, const Eigen::VectorXd& v,
              const Eigen::VectorXd& u, const Eigen::VectorXd& bb,
              Eigen::VectorXd& x)
{
    // size of R, which is size of u, v, and size of bb is n+1
    const unsigned int n = R.rows();

    // TODO: (2-9.d)
    // i) Use assert() to check that R, v, u, bb all have the appropriate sizes.
    // ii) Use (3-9.b) to solve the LSE.
    // Hint: Use R.triangularView<Eigen::Upper>() to make use of the triangular
    // structure of R.
    // START

    assert(R.cols() == n);
    assert(u.rows() == n);
    assert(v.rows() == n);
    assert(bb.rows() == n + 1);
    x.resize(n + 1, 1);
    constexpr double EPS = 1e-12;

    ei::TriangularView<const ei::MatrixXd, ei::Upper> Rtrig = R.triangularView<ei::Upper>();

    auto b = bb.head(n);
    double beta = bb(n);

    ei::VectorXd RinvB = Rtrig.solve(b);
    ei::BDCSVD<ei::MatrixXd> rSvd;
    bool svdComputed;
    rSvd.setThreshold(EPS);

    if (b.norm() != 0.0 && (b - Rtrig * RinvB).norm() / b.norm() > EPS)
    {
        rSvd.compute(R, ei::ComputeThinU | ei::ComputeThinV);
        svdComputed = true;
        RinvB = rSvd.solve(b);
    }

    ei::VectorXd RinvV = Rtrig.solve(v);
    if (v.norm() != 0.0 && (v - Rtrig * RinvV).norm() / v.norm() > EPS)
    {
        if (!svdComputed)
        {
            rSvd.compute(R, ei::ComputeThinU | ei::ComputeThinV);
        }
        RinvV = rSvd.solve(v);
    }

    std::cout << "RinvB error: " << (b - Rtrig * RinvB).norm() / b.norm() << std::endl;
    std::cout << "RinvV error: " << (v - Rtrig * RinvV).norm() / v.norm() << std::endl;
    std::cout << "u^T RinvB - beta: " << std::abs(u.dot(RinvB) - beta) << std::endl;
    std::cout << "RinvB - R^-1 v: " << (RinvB - x(n) * RinvV).norm() << std::endl;

    x(n) = (u.dot(RinvB) - beta) / u.dot(RinvV);
    x.head(n).noalias() = RinvB - x(n) * RinvV;

    // END
}
/* SAM_LISTING_END_1 */

/**
 * @brief Use Eigen's LU-solver to solve Ax = y and check against solvelse()
 *
 * @param R MatrixXd is nxn and upper triangular
 * @param v VectorXd is nx1
 * @param u VectorXd is nx1
 * @param b vector is (n+1)x1 and is stacked $(b, \beta)^T =: b$
 * @param x solution A*bb = x
 * @return true if the result of Eigen's solver conforms with solvelse()
 * @return false otherwise
 */
/* SAM_LISTING_BEGIN_2 */
bool testSolveLSE(const Eigen::MatrixXd& R, const Eigen::VectorXd& v,
                  const Eigen::VectorXd& u, const Eigen::VectorXd& b,
                  Eigen::VectorXd& x)
{
    bool areTheSame = false;

    // TODO: (2-9.e)
    // i) Create the system matrix A and solve the LSE, using
    // an Eigen LU-solver. Store the solution in x.
    // ii) Solve the LSE with solvelse(), and calculate the
    // difference between this solution and x. Return true
    // if and only if the norm of this difference is close
    // enough to zero.
    // START

    assert(R.rows() == R.cols());
    const ei::Index n = R.rows();

    ei::MatrixXd mat = ei::MatrixXd::Zero(n + 1, n + 1);
    mat.block(0, 0, n, n).triangularView<ei::Upper>() = R.triangularView<ei::Upper>();
    mat.col(n).head(n) = v;
    mat.row(n).head(n) = u.transpose();

    x = mat.lu().solve(b);
    ei::VectorXd customSol;

    solvelse(R, v, u, b, customSol);

    constexpr double EPS = 1e-12;
    areTheSame = (x - customSol).norm() < EPS * x.norm();

    // END
    return areTheSame;
}
/* SAM_LISTING_END_2 */

#endif
